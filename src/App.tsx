import { useState } from "react";

import ResultDisplay from "./components/ResultDisplay";
import CalculatorKeypad from "./container/CalculatorKeypad";

import { Flex } from "@chakra-ui/react";

function App() {
  const [equation, setEquation] = useState("");
  const [screenDisplay, setScreenDisplay] = useState("");

  function handleClick(value: string): void {
    if (value === "Enter") {
      let result = eval(equation);
      console.log(result);
      setScreenDisplay(result);
      return;
    } else if (value === "clear") {
      setEquation("");
      setScreenDisplay("");
      return;
    } else if (screenDisplay !== equation) {
      setEquation(value);
      setScreenDisplay(value);
      return;
    }
    setEquation((equation) => {
      const updatedEquation = equation.concat(value);
      setScreenDisplay(updatedEquation);
      return updatedEquation;
    });
  }

  console.log("Screen Display: ", screenDisplay);
  console.log("Equation: ", equation);

  return (
    <Flex
      w={"100vw"}
      h={"100vh"}
      bg={"gray"}
      display={"flex"}
      alignItems={"center"}
      justifyContent={"center"}
    >
      <Flex
        bg={"whiteAlpha.900"}
        borderRadius={"10px"}
        flexDirection={"column"}
        w={{ sm: "80vw", md: "60vw" }}
      >
        <ResultDisplay show={screenDisplay} />
        <CalculatorKeypad handleClick={handleClick} />
      </Flex>
    </Flex>
  );
}

export default App;
