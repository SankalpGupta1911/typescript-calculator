import { Card, CardBody, Text } from "@chakra-ui/react";

interface ResultDisplayProps {
  show: string;
}

const ResultDisplay: React.FC<ResultDisplayProps> = (props) => {
  const { show } = props;
  return (
    <Card bg="whitesmoke">
      <CardBody>
        <Text textAlign={"center"}>{show}</Text>
      </CardBody>
    </Card>
  );
};

export default ResultDisplay;
