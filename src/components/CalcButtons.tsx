import { Button } from "@chakra-ui/react";

interface ButtonProps {
  value: string;
  handleClick: any;
}

function CalcButtons({ value, handleClick }: ButtonProps) {
  return (
    <Button
      w={"20%"}
      margin={"2.5% 1.5% 2.5% 1.5%"}
      colorScheme="blue"
      onClick={() => handleClick(value)}
    >
      {value}
    </Button>
  );
}

export default CalcButtons;
