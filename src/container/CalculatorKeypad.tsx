import { Button, Flex } from "@chakra-ui/react";

import CalcButtons from "../components/CalcButtons";
const AllButtons: string[] = [
  "1",
  "2",
  "3",
  "+",
  "4",
  "5",
  "6",
  "-",
  "7",
  "8",
  "9",
  "*",
  "clear",
  "0",
  ".",
  "/",
];

function CalculatorKeypad({ handleClick }: any) {
  return (
    <Flex wrap={"wrap"} justifyContent={"center"}>
      {AllButtons.map((calcButton) => {
        return (
          <CalcButtons
            key={calcButton}
            value={calcButton}
            handleClick={handleClick}
          />
        );
      })}
      <Button
        w={"90%"}
        margin={"2.5% 1.5% 2.5% 1.5%"}
        colorScheme="red"
        onClick={() => {
          handleClick("Enter");
        }}
      >
        ENTER
      </Button>
    </Flex>
  );
}

export default CalculatorKeypad;
